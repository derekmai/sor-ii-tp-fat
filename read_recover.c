#include <stdio.h>
#include <stdlib.h>
#include "readHeaders.h"
#include <string.h>

void readFile(unsigned short primerClusterArchivo, unsigned short primerCluster, int fileSize, unsigned short clusterSize){
    FILE * in = fopen("test.img", "rb");
    int i;
    char leer[fileSize];
    
    fseek(in, primerCluster + ((primerClusterArchivo - 2) * clusterSize) , SEEK_SET); // Ir al inicio de la tabla de particiones
    fread(leer, fileSize, 1, in); // leo entradas 
    
    for(i=0; i<fileSize; i++) {
            printf("%c", leer[i]);
    }
    printf("\n");
    fclose(in);
}

void print_file_info(Fat12Entry *entry, unsigned short firstCluster, unsigned short clusterSize,char* searchingFilename) {
    switch(entry->filename[0]) {
    case 0x00:
        return; // unused entry.
    case 0xE5:
        printf("Archivo borrado: [?%.7s.%.3s]\n",entry->name,entry->extension);
        if (strstr(entry->name,searchingFilename) != NULL){
            printf("%s esta contenido en %s. Leyendo contenido\n",searchingFilename,entry->name);
            readFile(entry->cluster_lowbytes_address,firstCluster,entry->size_of_file, clusterSize);
        }
        else {
            printf("no hubo match en el filename.\n");
        }
        return; // erased entry.
    case 0x05: 
        return; // erased entry.
    default:
        switch(entry->attributes[0]) {
            case 0x10:
                return;
            case 0x20:
                printf("Archivo: [%.1s%.7s.%.3s]: ",  entry->filename, entry->name, entry->extension);
                readFile(entry->cluster_lowbytes_address,firstCluster,entry->size_of_file, clusterSize);
                return;
        }
    }
    
}

int main() {
    FILE * in = fopen("test.img", "rb");
    int i;
    PartitionTable pt[4];
    Fat12BootSector bs;
    Fat12Entry entry;

    unsigned short primerCluster;


    fseek(in, 446, SEEK_SET); // Ir al inicio de la tabla de particiones
    fread(pt, sizeof(PartitionTable), 4, in); // leo entradas 
    
    for(i=0; i<4; i++) {        
        if(pt[i].partition_type == 1) {
            printf("Encontrada particion FAT12 %d\n", i);
            break;
        }
    }
    
    if(i == 4) {
        printf("No encontrado filesystem FAT12, saliendo...\n");
        return -1;
    }

    fseek(in, 0, SEEK_SET);
    fread(&bs, sizeof(Fat12BootSector), 1, in);
           
    fseek(in, (bs.reserved_sectors-1 + bs.fat_size_sectors * bs.number_of_fats) *
          bs.sector_size, SEEK_CUR);

    primerCluster = ftell(in) + (bs.root_dir_entries * sizeof(entry));

    printf("Ingrese parte del filename del archivo que quiere buscar y leer su contenido.\n");
    char filenameToTryToFind[50] = "";
    gets(filenameToTryToFind);
    printf("Se buscara un match entre los filename y el string %s.\n",filenameToTryToFind);
    for(i=0; i<bs.root_dir_entries; i++) {
        fread(&entry, sizeof(entry), 1, in);
        print_file_info(&entry, primerCluster, bs.sector_size * bs.sector_cluster,filenameToTryToFind);
    }

    fclose(in);
    return 0;
}