# SOR II - TP FAT

SOR II (Sistemas Operativos y Redes II). Tp sobre FAT (File Allocation Table).

## Requerimientos
Poner en práctica los conocimientos sobre el diseño de las FAT. Para esto se debió escribir código con el fin de leer distintas partes de un filesystem FAT.
- MBR (Master Boot Record).
- Información sobre la primer partición del filesystem.
- Lectura de archivos y directorios.
- Recuperación de Archivos.

Mucho del proceso requerido está explicado en detalle en el informe entregado junto al código fuente, con texto e imagenes explicativas, así que decidí dejarlo en el repositorio.
