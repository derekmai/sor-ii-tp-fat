#include <stdio.h>
#include <stdlib.h>
#include "readHeaders.h"

int main() {
    FILE * in = fopen("test.img", "rb");
    PartitionTable partitionTable[4];
    fseek(in, 0x1BE , SEEK_SET);
    fread (partitionTable,sizeof(PartitionTable),4,in);

    int i;
    for(i=0; i<4; i++) {
        printf("Partition entry %d: First byte %02X\n", i, partitionTable[i].first_byte);
        printf("  Comienzo de partición en CHS: %02X:%02X:%02X\n",
        partitionTable[i].start_chs[2],
        partitionTable[i].start_chs[1],
        partitionTable[i].start_chs[0]);
        printf("  Partition type 0x%02X\n", partitionTable[i].partition_type);
        printf("  Fin de partición en CHS: %02X:%02X:%02X\n",
        partitionTable[i].end_chs[0],
        partitionTable[i].end_chs[1],
        partitionTable[i].end_chs[2]);
        printf("  Dirección LBA relativa 0x%08X, de tamaño en sectores %d\n", partitionTable[i].start_sector[0],
        partitionTable[i].length_sectors);
    }
    
    fclose(in);
    return 0;
}
